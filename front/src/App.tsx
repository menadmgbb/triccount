import React from 'react';
import './App.css';
import MainRoutes from './components/Routes.component';


class App extends React.Component {

    render() {
        return (
            <div>
                <MainRoutes/>
            </div>
        );
    }
}

export default App;
